var time = $('#nowtime');
var runimg = $('#runimg');

function ShowTime() {
    var NowDate = new Date();
    var h = NowDate.getHours();
    var m = NowDate.getMinutes();
    var s = NowDate.getSeconds();
    time.text(`${h} : ${m} : ${s}`)
    setTimeout('ShowTime()', 1000);
}
ShowTime();

function addZero(i) {
    if (i < 10) i = "0" + i;
    return i;
}

var token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImU1MTRjOTMwZmVjMGQxZTk5MDNhOWVlODVmM2JmMzRmMjlhMzhhMDg3ZGUwOTMyZDA0OWE5ZGFhMTU5YzljOTdlYWY3MTFmZTg2NTgwMTZmIn0.eyJhdWQiOiIyIiwianRpIjoiZTUxNGM5MzBmZWMwZDFlOTkwM2E5ZWU4NWYzYmYzNGYyOWEzOGEwODdkZTA5MzJkMDQ5YTlkYWExNTljOWM5N2VhZjcxMWZlODY1ODAxNmYiLCJpYXQiOjE1NjAxNTQ2OTEsIm5iZiI6MTU2MDE1NDY5MSwiZXhwIjoxNTkxNzc3MDkxLCJzdWIiOiIyNyIsInNjb3BlcyI6W119.nQK43BdD5sfwyKMRD0DJeE9YfKlsKgtiFeRCQTMQKIQfxoS8Py1WDKR_hso4PujpeSRRbS2UYDtbAFSBBxfI4q7PvJcNpWThl_ezhgktT4gms-Ol6P-sekj5l-dMz33OG7AgGojWPIOfDy3vC1hQD2l2bW17fBknizG8Hu94Qo74wvWWh4L9PHFLnqd-nGvXCj8wzrChlQXFsciFIuwdXurU9JGFnKLW4P05pVZgeoHy0pa2tzPav9wvYbgfMrnjcO8NhRw3i6pFRthAE3pE9GNF8LRqdf3yGyMi3DT0HnAXkInNx_rgOvvyG9jOyC5UF31y7XLo3SeVktJMkzd9K72A7iqfvqJIeM6ETkvgtdTiFP4fLaOJihxgqiEYPlN4FPnruUCr7KKjEA1jnLX6PnWGWghdWC-8e8uWlqaI8lq-ZqxgIpzfM4YMwlDXHK5zmlTa1OLVdnBR5lQU6cUHbQunmVHqcu4ypg5IQk6eMzLxw3YpKdu_7xPlylghuLe3U1Vsee6ThNzgerdeApCL8KjLZguARfv5K3Y4UrOtGt5FEVfnuq82h6vEcIWCfp02x7yA82udb_IsJXroHFJ1CZ4g5OxuCr3Rsy38iIFiey-cOO4RNshi18qvPZlruyttR4MqsQyxhYz1XZXNNHVfZjLZFhCw4Pcd14ivFpOvVW0';
$.ajax({
    type: 'post',
    url: 'https://campus.kits.tw/api/get/data/aa44071d',
    headers: {
        'Access-Control-Allow-Origin': '*',
        "Authorization": 'Bearer ' + token,
        'Accept': 'application/json'
    },

    success: function (alldata) {
        var data = alldata[alldata.length - 1];
        var lasttime = data.created_at[8] + data.created_at[9] + data.created_at[11] + data.created_at[12] + data.created_at[14] + data.created_at[15] + data.created_at[17] + data.created_at[18];
        var nowtime = addZero(new Date().getDate().toString())+addZero(new Date().getHours().toString())+addZero(new Date().getMinutes().toString())+addZero(new Date().getSeconds().toString());
        
        if (lasttime.substring(0, 2) == nowtime.substring(0, 2)) { //check day
            if (Number(nowtime.substring(2,4)) - Number(lasttime.substring(2,4)) <= 1) { //check hour(0-1)
                if (Number(nowtime[3]) - Number(lasttime[3]) == 0) { //same hour
                    if (Number(nowtime[4]) - Number(lasttime[4]) <= 2 && Number(nowtime[4]) - Number(lasttime[4]) >= 0) {
                        runimg.attr({
                            "src": "photo/runred.gif"
                        });
                    }
                } else {
                    if (Number(nowtime[4]) + 6 - Number(lasttime[4]) <= 2 && Number(nowtime[4]) + 6 - Number(lasttime[4]) >= 0) { //diff hour
                        runimg.attr({
                            "src": "photo/runred.gif"
                        });
                    }
                }
            }
        }
        console.log(lasttime)
        console.log(nowtime)
    },
    error: function (err) {
        console.log(err)
    }
})