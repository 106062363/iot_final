function dayuse(thedate) {
    var token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImU1MTRjOTMwZmVjMGQxZTk5MDNhOWVlODVmM2JmMzRmMjlhMzhhMDg3ZGUwOTMyZDA0OWE5ZGFhMTU5YzljOTdlYWY3MTFmZTg2NTgwMTZmIn0.eyJhdWQiOiIyIiwianRpIjoiZTUxNGM5MzBmZWMwZDFlOTkwM2E5ZWU4NWYzYmYzNGYyOWEzOGEwODdkZTA5MzJkMDQ5YTlkYWExNTljOWM5N2VhZjcxMWZlODY1ODAxNmYiLCJpYXQiOjE1NjAxNTQ2OTEsIm5iZiI6MTU2MDE1NDY5MSwiZXhwIjoxNTkxNzc3MDkxLCJzdWIiOiIyNyIsInNjb3BlcyI6W119.nQK43BdD5sfwyKMRD0DJeE9YfKlsKgtiFeRCQTMQKIQfxoS8Py1WDKR_hso4PujpeSRRbS2UYDtbAFSBBxfI4q7PvJcNpWThl_ezhgktT4gms-Ol6P-sekj5l-dMz33OG7AgGojWPIOfDy3vC1hQD2l2bW17fBknizG8Hu94Qo74wvWWh4L9PHFLnqd-nGvXCj8wzrChlQXFsciFIuwdXurU9JGFnKLW4P05pVZgeoHy0pa2tzPav9wvYbgfMrnjcO8NhRw3i6pFRthAE3pE9GNF8LRqdf3yGyMi3DT0HnAXkInNx_rgOvvyG9jOyC5UF31y7XLo3SeVktJMkzd9K72A7iqfvqJIeM6ETkvgtdTiFP4fLaOJihxgqiEYPlN4FPnruUCr7KKjEA1jnLX6PnWGWghdWC-8e8uWlqaI8lq-ZqxgIpzfM4YMwlDXHK5zmlTa1OLVdnBR5lQU6cUHbQunmVHqcu4ypg5IQk6eMzLxw3YpKdu_7xPlylghuLe3U1Vsee6ThNzgerdeApCL8KjLZguARfv5K3Y4UrOtGt5FEVfnuq82h6vEcIWCfp02x7yA82udb_IsJXroHFJ1CZ4g5OxuCr3Rsy38iIFiey-cOO4RNshi18qvPZlruyttR4MqsQyxhYz1XZXNNHVfZjLZFhCw4Pcd14ivFpOvVW0';
    var lasttime = 0;

    var year = thedate.substring(0, 4);
    var month = thedate.substring(4 ,6);
    var day = thedate.substring(6 , 8);

    $.ajax({
        type: 'post',
        url: `https://campus.kits.tw/api/get/data/aa44071d?date_filter=${year}-${month}-${day} +-+${year}-${month}-${day+1}`,
        headers: {
            'Access-Control-Allow-Origin': '*',
            "Authorization": 'Bearer ' + token,
            'Accept': 'application/json'
        },

        success: function (alldata) {
            console.log(alldata)
            var shake = new Array(24).fill(0);
            alldata.forEach(data => {
                var datatime = parseInt(data.created_at[8] + data.created_at[9] + data.created_at[11] + data.created_at[12] + data.created_at[14] + data.created_at[15] + data.created_at[17] + data.created_at[18]);
                if (datatime - lasttime >= 1000) {
                    thehour = Number(data.created_at[11] + data.created_at[12]);
                    shake[thehour]++;
                    console.log(data.created_at);
                    lasttime = datatime;
                }
            });
            console.log(shake);

            Chart.defaults.global.defaultFontFamily = 'Lato';
            //Chart.defaults.global.defaultFontSize = 18;
            Chart.defaults.global.defaultFontColor = 'black';
            massPopChart = new Chart(ctx, {
                type: 'horizontalBar', // bar, horizontalBar, pie, line, doughnut, radar, polarArea
                data: {
                    labels: ['6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23'],
                    datasets: [{
                        label: 'Population',
                        data: [shake[6], shake[7], shake[8], shake[9], shake[10], shake[11], shake[12], shake[13], shake[14], shake[15], shake[16], shake[17], shake[18], shake[19], shake[20], shake[21], shake[22], shake[23]],
                        backgroundColor: [
                            useratecolor(shake[6]),
                            useratecolor(shake[7]),
                            useratecolor(shake[8]),
                            useratecolor(shake[9]),
                            useratecolor(shake[10]),
                            useratecolor(shake[11]),
                            useratecolor(shake[12]),
                            useratecolor(shake[13]),
                            useratecolor(shake[14]),
                            useratecolor(shake[15]),
                            useratecolor(shake[16]),
                            useratecolor(shake[17]),
                            useratecolor(shake[18]),
                            useratecolor(shake[19]),
                            useratecolor(shake[20]),
                            useratecolor(shake[21]),
                            useratecolor(shake[22]),
                            useratecolor(shake[23])
                        ],
                        borderWidth: 1,
                        borderColor: '#777',
                        hoverBorderWidth: 3,
                        hoverBorderColor: '#000'
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            scaleLabel: {
                                display: true,
                                labelString: '小時'
                            }
                        }],
                        xAxes: [{
                            scaleLabel: {
                                display: true,
                                labelString: '使用率'
                            }
                        }]
                    },
                    title: {
                        display: true,
                        text: '某天跑步機使用率',
                        fontSize: 20
                    },
                    legend: {
                        display: false,
                        position: 'right',
                        labels: {
                            fontColor: '#000'
                        }
                    },
                    layout: {
                        padding: {
                            left: 100,
                            right: 100,
                            bottom: 0,
                            top: 0
                        }
                    },
                    tooltips: {
                        enabled: true
                    },
                    "hover": {
                        "animationDuration": 0
                    },
                    "animation": {
                        "duration": 1,
                        "onComplete": function () {
                            var chartInstance = this.chart,
                                ctx = chartInstance.ctx;

                            ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'bottom';

                            this.data.datasets.forEach(function (dataset, i) {
                                var meta = chartInstance.controller.getDatasetMeta(i);
                                meta.data.forEach(function (bar, index) {
                                    var data = dataset.data[index];
                                    ctx.fillText(data, bar._model.x+10, bar._model.y+5);
                                });
                            });
                        }
                    }
                }
            });
        },
        error: function (err) {
            console.log(err)
        }
    })
}


function weekuse() {
    var token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImU1MTRjOTMwZmVjMGQxZTk5MDNhOWVlODVmM2JmMzRmMjlhMzhhMDg3ZGUwOTMyZDA0OWE5ZGFhMTU5YzljOTdlYWY3MTFmZTg2NTgwMTZmIn0.eyJhdWQiOiIyIiwianRpIjoiZTUxNGM5MzBmZWMwZDFlOTkwM2E5ZWU4NWYzYmYzNGYyOWEzOGEwODdkZTA5MzJkMDQ5YTlkYWExNTljOWM5N2VhZjcxMWZlODY1ODAxNmYiLCJpYXQiOjE1NjAxNTQ2OTEsIm5iZiI6MTU2MDE1NDY5MSwiZXhwIjoxNTkxNzc3MDkxLCJzdWIiOiIyNyIsInNjb3BlcyI6W119.nQK43BdD5sfwyKMRD0DJeE9YfKlsKgtiFeRCQTMQKIQfxoS8Py1WDKR_hso4PujpeSRRbS2UYDtbAFSBBxfI4q7PvJcNpWThl_ezhgktT4gms-Ol6P-sekj5l-dMz33OG7AgGojWPIOfDy3vC1hQD2l2bW17fBknizG8Hu94Qo74wvWWh4L9PHFLnqd-nGvXCj8wzrChlQXFsciFIuwdXurU9JGFnKLW4P05pVZgeoHy0pa2tzPav9wvYbgfMrnjcO8NhRw3i6pFRthAE3pE9GNF8LRqdf3yGyMi3DT0HnAXkInNx_rgOvvyG9jOyC5UF31y7XLo3SeVktJMkzd9K72A7iqfvqJIeM6ETkvgtdTiFP4fLaOJihxgqiEYPlN4FPnruUCr7KKjEA1jnLX6PnWGWghdWC-8e8uWlqaI8lq-ZqxgIpzfM4YMwlDXHK5zmlTa1OLVdnBR5lQU6cUHbQunmVHqcu4ypg5IQk6eMzLxw3YpKdu_7xPlylghuLe3U1Vsee6ThNzgerdeApCL8KjLZguARfv5K3Y4UrOtGt5FEVfnuq82h6vEcIWCfp02x7yA82udb_IsJXroHFJ1CZ4g5OxuCr3Rsy38iIFiey-cOO4RNshi18qvPZlruyttR4MqsQyxhYz1XZXNNHVfZjLZFhCw4Pcd14ivFpOvVW0';
    var lasttime = 0;
    var today = new Date().getDate();
    //console.log(`https://campus.kits.tw/api/get/data/aa44071d?date_filter=2019-06-${today} +-+2019-06-${today-7}`)
    console.log(today);
    $.ajax({
        type: 'post',
        url: `https://campus.kits.tw/api/get/data/aa44071d?date_filter=2019-06-${today-10} +-+2019-06-${today}`,
        headers: {
            'Access-Control-Allow-Origin': '*',
            "Authorization": 'Bearer ' + token,
            'Accept': 'application/json'
        },

        success: function (alldata) {
            console.log(alldata)
            var shake = new Array(31).fill(0);
            alldata.forEach(data => {
                var datatime = parseInt(data.created_at[8] + data.created_at[9] + data.created_at[11] + data.created_at[12] + data.created_at[14] + data.created_at[15] + data.created_at[17] + data.created_at[18]);
                if (datatime - lasttime >= 1000) {
                    theday = Number(data.created_at[8] + data.created_at[9]);
                    shake[theday]++;
                    console.log(data.created_at);
                    lasttime = datatime;
                }
            });
            console.log(shake);

            Chart.defaults.global.defaultFontFamily = 'Lato';
            //Chart.defaults.global.defaultFontSize = 18;
            Chart.defaults.global.defaultFontColor = 'black';
            massPopChart = new Chart(ctx, {
                type: 'horizontalBar', // bar, horizontalBar, pie, line, doughnut, radar, polarArea
                data: {
                    labels: [(today - 10).toString(),(today - 9).toString(),(today - 8).toString(),(today - 7).toString(), (today - 6).toString(), (today - 5).toString(), (today - 4).toString(), (today - 3).toString(), (today - 2).toString(), (today - 1).toString()],
                    datasets: [{
                        label: 'Population',
                        data: [shake[today - 10],shake[today - 9],shake[today - 8],shake[today - 7], shake[today - 6], shake[today - 5], shake[today - 4], shake[today - 3], shake[today - 2], shake[today - 1]],
                        backgroundColor: [
                            'rgba(54, 162, 235)',
                            'rgba(255, 206, 86)',
                            'rgba(75, 192, 192)',
                            'rgba(153, 102, 255)',
                            'rgba(255, 159, 64)',
                            'rgba(255, 99, 132)',
                            'rgba(255, 50, 70)',
                            'rgba(54, 162, 235)',
                            'rgba(255, 206, 86)',
                            'rgba(75, 192, 192)',
                            'rgba(153, 102, 255)',
                            'rgba(255, 159, 64)',
                            'rgba(255, 99, 132)',
                            'rgba(75, 192, 192)'

                        ],
                        borderWidth: 1,
                        borderColor: '#777',
                        hoverBorderWidth: 3,
                        hoverBorderColor: '#000'
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            scaleLabel: {
                                display: true,
                                labelString: '過去十天日期'
                            }
                        }],
                        xAxes: [{
                            scaleLabel: {
                                display: true,
                                labelString: '使用率'
                            }
                        }]
                    },

                    title: {
                        display: true,
                        text: '過去十天使用率',
                        fontSize: 20
                    },
                    legend: {
                        display: false,
                        position: 'right',
                        labels: {
                            fontColor: '#000'
                        }
                    },
                    layout: {
                        padding: {
                            left: 100,
                            right: 100,
                            bottom: 0,
                            top: 0
                        }
                    },
                    tooltips: {
                        enabled: true
                    },
                    "hover": {
                        "animationDuration": 0
                    },
                    "animation": {
                        "duration": 1,
                        "onComplete": function () {
                            var chartInstance = this.chart,
                                ctx = chartInstance.ctx;

                            ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'bottom';

                            this.data.datasets.forEach(function (dataset, i) {
                                var meta = chartInstance.controller.getDatasetMeta(i);
                                meta.data.forEach(function (bar, index) {
                                    var data = dataset.data[index];
                                    ctx.fillText(data, bar._model.x+10, bar._model.y+5);
                                });
                            });
                        }
                    }
                },


            });

            massPopChart.onAnimationComplete;
        },
        error: function (err) {
            console.log(err)
        }
    })
}


function allusehigh() {
    var token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImU1MTRjOTMwZmVjMGQxZTk5MDNhOWVlODVmM2JmMzRmMjlhMzhhMDg3ZGUwOTMyZDA0OWE5ZGFhMTU5YzljOTdlYWY3MTFmZTg2NTgwMTZmIn0.eyJhdWQiOiIyIiwianRpIjoiZTUxNGM5MzBmZWMwZDFlOTkwM2E5ZWU4NWYzYmYzNGYyOWEzOGEwODdkZTA5MzJkMDQ5YTlkYWExNTljOWM5N2VhZjcxMWZlODY1ODAxNmYiLCJpYXQiOjE1NjAxNTQ2OTEsIm5iZiI6MTU2MDE1NDY5MSwiZXhwIjoxNTkxNzc3MDkxLCJzdWIiOiIyNyIsInNjb3BlcyI6W119.nQK43BdD5sfwyKMRD0DJeE9YfKlsKgtiFeRCQTMQKIQfxoS8Py1WDKR_hso4PujpeSRRbS2UYDtbAFSBBxfI4q7PvJcNpWThl_ezhgktT4gms-Ol6P-sekj5l-dMz33OG7AgGojWPIOfDy3vC1hQD2l2bW17fBknizG8Hu94Qo74wvWWh4L9PHFLnqd-nGvXCj8wzrChlQXFsciFIuwdXurU9JGFnKLW4P05pVZgeoHy0pa2tzPav9wvYbgfMrnjcO8NhRw3i6pFRthAE3pE9GNF8LRqdf3yGyMi3DT0HnAXkInNx_rgOvvyG9jOyC5UF31y7XLo3SeVktJMkzd9K72A7iqfvqJIeM6ETkvgtdTiFP4fLaOJihxgqiEYPlN4FPnruUCr7KKjEA1jnLX6PnWGWghdWC-8e8uWlqaI8lq-ZqxgIpzfM4YMwlDXHK5zmlTa1OLVdnBR5lQU6cUHbQunmVHqcu4ypg5IQk6eMzLxw3YpKdu_7xPlylghuLe3U1Vsee6ThNzgerdeApCL8KjLZguARfv5K3Y4UrOtGt5FEVfnuq82h6vEcIWCfp02x7yA82udb_IsJXroHFJ1CZ4g5OxuCr3Rsy38iIFiey-cOO4RNshi18qvPZlruyttR4MqsQyxhYz1XZXNNHVfZjLZFhCw4Pcd14ivFpOvVW0';
    var lasttime = 0;

    $.ajax({
        type: 'post',
        url: `https://campus.kits.tw/api/get/data/aa44071d`,
        headers: {
            'Access-Control-Allow-Origin': '*',
            "Authorization": 'Bearer ' + token,
            'Accept': 'application/json'
        },

        success: function (alldata) {
            console.log(alldata)
            var shake = new Array(24).fill(0);
            alldata.forEach(data => {
                var datatime = parseInt(data.created_at[8] + data.created_at[9] + data.created_at[11] + data.created_at[12] + data.created_at[14] + data.created_at[15] + data.created_at[17] + data.created_at[18]);
                if (datatime - lasttime >= 1000) {
                    thehour = Number(data.created_at[11] + data.created_at[12]);
                    shake[thehour]++;
                    console.log(data.created_at);
                    lasttime = datatime;
                }
            });
            console.log(shake);

            Chart.defaults.global.defaultFontFamily = 'Lato';
            //Chart.defaults.global.defaultFontSize = 18;
            Chart.defaults.global.defaultFontColor = 'black';
            massPopChart = new Chart(ctx, {
                type: 'pie', // bar, horizontalBar, pie, line, doughnut, radar, polarArea
                
                data: {
                    labels: ['6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23'],
                    datasets: [{
                        data: [shake[6], shake[7], shake[8], shake[9], shake[10], shake[11], shake[12], shake[13], shake[14], shake[15], shake[16], shake[17], shake[18], shake[19], shake[20], shake[21], shake[22], shake[23]],
                        backgroundColor: [
                            'rgba(54, 162, 235)',
                            'rgba(255, 206, 86)',
                            'rgba(75, 192, 192)',
                            'rgba(153, 102, 255)',
                            'rgba(210, 159, 64)',
                            'rgba(111, 99, 132)',
                            'rgba(56, 50, 70)',
                            'rgba(54, 162, 235)',
                            'rgba(48, 206, 86)',
                            'rgba(75, 192, 192)',
                            'rgba(153, 102, 21)',
                            'rgba(24, 159, 64)',
                            'rgba(150, 99, 132)',
                            'rgba(75, 192, 192)',
                            'rgba(60, 159, 64)',
                            'rgba(120, 99, 55)',
                            'rgba(100, 50, 70)',
                            'rgba(54, 162, 235)',
                        ],
                        borderWidth: 1,
                        borderColor: '#777',
                        hoverBorderWidth: 3,
                        hoverBorderColor: '#000'
                    }],

                    options: {
    
                        title: {
                            display: true,
                            text: '過去七天使用率',
                            fontSize: 20
                        },
                        legend: {
                            display: false,
                            position: 'right',
                            labels: {
                                fontColor: '#000'
                            }
                        },
                    },
                },
                
            });
        },
        error: function (err) {
            console.log(err)
        }
    })
}


function useratecolor(rate) {
    var thecolor = '';
    switch (rate) {
        case 1:
            thecolor = 'rgb(0, 255, 0)';
            break;
        case 2:
            thecolor = 'rgb(0, 180, 0)';
            break;
        case 3:
            thecolor = 'rgb(255, 255, 0)';
            break;
        case 4:
            thecolor = 'rgb(255, 180, 0)';
            break;
        case 5:
            thecolor = 'rgb(255, 100, 0)';
            break;
        case 6:
            thecolor = 'rgb(255, 0, 0)';
            break;
        default:
            break;
    }
    return thecolor;
}

var theweeduse = $('#theweeduse');
var thedayuse = $('#thedayuse');
var thehigh = $('#thehigh');
var datecon = $('#datecon');
var date = $('#date');
var myChart1 = document.getElementById('myChart')
var ctx = myChart1.getContext('2d');
var massPopChart = 0;
var chart1 = $('#chart1');

theweeduse.click(()=>{
    //chart1.css('width','80%');
    if(screen.width>=750)chart1.css('width','80%');
    else chart1.css('width','100%');
    datecon.css('visibility','hidden');
    if(massPopChart != 0){
        massPopChart.destroy();
    }
    weekuse();
});

thedayuse.click(()=>{
    //chart1.css('width','80%');
    if(screen.width>=750)chart1.css('width','80%');
    else chart1.css('width','100%');
    datecon.css('visibility','initial');
    if(massPopChart != 0){
        massPopChart.destroy();
    }
});

thehigh.click(()=>{
    if(screen.width>=750)chart1.css('width','40%');
    datecon.css('visibility','hidden');
    if(massPopChart != 0){
        massPopChart.destroy();
    }
    allusehigh();
});

date.change(()=>{
    if(massPopChart != 0){
        massPopChart.destroy();
    }
    var re = /-/gi;
    var thedate = date.val();
    thedate = thedate.replace(re,'');
    dayuse(thedate);
});
